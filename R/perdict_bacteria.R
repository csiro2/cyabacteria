#' predict_bacteria
#' 
#' @param data data.frame
#' @param perdictors character vector, independent variables
#' @param response character vector, dependent variables
#' @param ... others to [randomForest_data]
#' 
#' @return 
#' - model
#' - `info`: Goodness-of-fitting infomation
#'   * NSE, RMSE, R2, ...
#' - `outout`:
#'   * `ySim`: predicted value in calibration 
#'   * `yVal`: predicted value in validation
#' @seealso [randomForest_data], [randomForest_val]
#' 
#' @export
predict_bacteria <- function(
    data, 
    predictors = c("date", "doy", "Velocity", "Temperature", "Salinity", "Discharge"), 
    response   = c("CYA_sum"), ...)
{
    # npredict   <- length(predictors)
    data2 <- data[, .SD, .SDcols = c(predictors, response, 'date')]
    data2 <- na.omit(data2)
    t     <- data2$date
    
    INPUT <- randomForest_data(data2, predictors, response, train_perc = 0.75, ...)
    # m     <- with(INPUT, randomForest(x_train, y_train, x_test, y_test, ntree = 500))
    # Only those part data was used.
    m     <- with(INPUT, randomForest(x_train, y_train, ntree = 500))
    ysim  <- predict(m, INPUT$X)
    # m     <- with(INPUT, randomForest(X, Y, ntree = 500))
    
    # res_cal <- randomForest_cal(INPUT)
    res_val <- randomForest_val(INPUT, kfold = 5)
    
    # browser()
    output = data.table(t, yObs = INPUT$Y[, 1], ySim = ysim, yVal = res_val$ypred[, 1])
    c(res_val[c(1, 3)], list(output = output)) # return
}
