---
title: "Cyanobacteria"
author: "Dongdong"
date: "21 November 2018"
output: 
    html_document:
        keep_md: true
---




```r
source('R/load_pkgs.R')

RF_gof <- function(predictors, response = "CYA_sum"){
    res  <- llply(lst, pred_Bacteria, predictors = predictors, response = response)
    
    d <- map(res, "output") %>% melt_list("site")
    info <- list(
        cal = d[yObs >= 2, as.list(GOF(yObs, ySim)), .(site)][, .(site, NSE, R2, RMSE, MAE, n_sim)],
        val = d[yObs >= 2, as.list(GOF(yObs, yVal)), .(site)][, .(site, NSE, R2, RMSE, MAE, n_sim)]
    ) %>% melt_list("type")
    info
}
```


```r
load("data/RF_input.rda")
df_t1  <- df_all[, add_tn(CYA_sum, yd7, 1:2, prefix = "CYA_sum"), .(site)]
df_all <- merge(df_all, df_t1, all.x = T)
df_all[, `:=`(
    PTwater = cal_PTwater(Temperature),
    PTwater2 = cal_PTwater(TempWater))]

# If considering upstream data, only 4 sites available.
# Add upstream `t-1` data
is_add_up_t1 <- FALSE
if (is_add_up_t1){
   INPUT_lst <- list()
    for (i in 1:(length(shortname)-1)){
        sitename    <- shortname[i+1]
        sitename_up <- shortname[i]
        
        d    <- df_all[site == sitename]
        d_up <- df_all[site == sitename_up]
        
        lag <- 1
        I   <- match(d_up$yd7 - 1, d$yd7) %>% {.[!is.na(.)]}
        
        d_up <- select(d_up, matches("yd7|CYA"))
        names <- colnames(d_up)
        colnames(d_up)[3:length(names)] <- names[3:length(names)] %>% paste0("_upt1")
        
        d_temp <- merge(d, d_up, all.x = T, by = "yd7")
        INPUT_lst[[i]] <- d_temp
    }
    df_all <- do.call(rbind, INPUT_lst) 
}

summary(df_all)
```

```
           site           yd7             year            d7       
 Lock9       :1083   Min.   :-1242   Min.   :1970   Min.   : 1.00  
 Morgan      :1135   1st Qu.:  162   1st Qu.:1997   1st Qu.:11.00  
 Mannum      : 936   Median :  601   Median :2005   Median :24.00  
 MurrayBridge: 658   Mean   :  513   Mean   :2003   Mean   :24.86  
 TailemBend  : 780   3rd Qu.:  923   3rd Qu.:2011   3rd Qu.:38.00  
                     Max.   : 1258   Max.   :2018   Max.   :52.00  
                                                                   
     CYA_I          CYA_IIIb        CYA_IIIp        CYA_IVb     
 Min.   : -Inf   Min.   : -Inf   Min.   : -Inf   Min.   :0.000  
 1st Qu.:1.732   1st Qu.:1.000   1st Qu.:1.422   1st Qu.:1.447  
 Median :2.483   Median :1.477   Median :1.808   Median :1.875  
 Mean   : -Inf   Mean   : -Inf   Mean   : -Inf   Mean   :1.783  
 3rd Qu.:3.377   3rd Qu.:1.927   3rd Qu.:2.205   3rd Qu.:2.274  
 Max.   :5.845   Max.   :3.804   Max.   :4.737   Max.   :3.876  
 NA's   :3774    NA's   :4256    NA's   :863     NA's   :3615   
    CYA_IVp         CYA_sum         Velocity       Temperature   
 Min.   : -Inf   Min.   :0.000   Min.   :0.0000   Min.   : 9.36  
 1st Qu.:1.518   1st Qu.:1.477   1st Qu.:0.0334   1st Qu.:14.00  
 Median :2.049   Median :2.002   Median :0.0621   Median :19.37  
 Mean   : -Inf   Mean   :2.078   Mean   :0.1042   Mean   :18.96  
 3rd Qu.:2.839   3rd Qu.:2.581   3rd Qu.:0.1168   3rd Qu.:23.66  
 Max.   :4.962   Max.   :5.845   Max.   :0.6455   Max.   :30.00  
 NA's   :3111                    NA's   :747      NA's   :810    
    Salinity        Discharge        ElecCond        TempWater    
 Min.   :  85.0   Min.   :    0   Min.   :  85.0   Min.   : 9.81  
 1st Qu.: 286.0   1st Qu.: 2868   1st Qu.: 282.5   1st Qu.:14.17  
 Median : 394.0   Median : 5031   Median : 385.6   Median :19.72  
 Mean   : 418.3   Mean   :10947   Mean   : 423.9   Mean   :19.07  
 3rd Qu.: 528.2   3rd Qu.:11423   3rd Qu.: 544.0   3rd Qu.:23.71  
 Max.   :1246.0   Max.   :81192   Max.   :1499.0   Max.   :29.85  
 NA's   :804      NA's   :747     NA's   :119      NA's   :14     
    FlowRate        Radiation          date               CYA_sum1    
 Min.   :     0   Min.   : 3.00   Min.   :1970-02-05   Min.   :0.000  
 1st Qu.:  2953   1st Qu.:11.00   1st Qu.:1997-02-05   1st Qu.:1.602  
 Median :  5556   Median :18.00   Median :2005-07-16   Median :2.106  
 Mean   : 13956   Mean   :18.18   Mean   :2003-11-05   Mean   :2.200  
 3rd Qu.: 15084   3rd Qu.:25.00   3rd Qu.:2011-09-24   3rd Qu.:2.704  
 Max.   :158881   Max.   :33.00   Max.   :2018-03-05   Max.   :5.845  
                                                       NA's   :1110   
    CYA_sum2        PTwater           PTwater2       
 Min.   :0.000   Min.   :-0.0935   Min.   :-0.02733  
 1st Qu.:1.602   1st Qu.: 0.4901   1st Qu.: 0.50699  
 Median :2.113   Median : 0.8878   Median : 0.90544  
 Mean   :2.198   Mean   : 0.7335   Mean   : 0.74839  
 3rd Qu.:2.705   3rd Qu.: 0.9868   3rd Qu.: 0.99216  
 Max.   :5.845   Max.   : 1.0000   Max.   : 1.00000  
 NA's   :1165    NA's   :810       NA's   :14        
```

```r
lst <- df_all %>% split(., .$site) # divide into sites

vars_env = c("Velocity", "Temperature", "Salinity", "Discharge", 
              "ElecCond", "TempWater", "FlowRate", "Radiation")
response = c("CYA_sum") #CYA_IIIp, CYA_sum
```

From the input data, we can see that `TempWater`, `ElecCond` and `FlowRate` have 
more 796 (17.3%), 685 (14.9%) and 747 (16.3%) observations available. 

## 1. Select variables

### 1.1 Velocity or Discharge   
Result shows that `Velocity` is more important than `Discharge`. Take `Lock9` as
example, when using `Velocity` other than `Discharge`, `NSE`can increase from 
0.156 to 0.174. For other sites, using `Velocity` also have a relatively large 
`NSE`.


```
$`yd7, Temperature, Salinity, Velocity`
            site         NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.70827242 0.74747386 0.4180716 0.3144365   651  cal
 2:       Morgan  0.63005957 0.70529995 0.4254632 0.3010108   534  cal
 3:       Mannum  0.33624239 0.62099406 0.3872705 0.2831688   319  cal
 4: MurrayBridge -0.09746892 0.39714104 0.4035918 0.2841660   189  cal
 5:   TailemBend  0.25941595 0.45781335 0.5927402 0.4031068   305  cal
 6:        Lock9  0.17369330 0.32447272 0.7036109 0.5533502   651  val
 7:       Morgan -0.06936289 0.26481075 0.7233669 0.5563805   534  val
 8:       Mannum -1.06302923 0.09044687 0.6827511 0.5320004   319  val
 9: MurrayBridge -1.80725772 0.05237189 0.6454868 0.5196895   189  val
10:   TailemBend -0.54469333 0.07733961 0.8560485 0.6309572   305  val

$`yd7, Temperature, Salinity, Discharge`
            site         NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.70865151 0.74934358 0.4177999 0.3097527   651  cal
 2:       Morgan  0.63073625 0.70999816 0.4250739 0.3063396   534  cal
 3:       Mannum  0.41488622 0.63850069 0.3636050 0.2699567   319  cal
 4: MurrayBridge  0.02263096 0.42194057 0.3808689 0.2678015   189  cal
 5:   TailemBend  0.53174698 0.67342661 0.4713218 0.3468230   305  cal
 6:        Lock9  0.15588952 0.31103988 0.7111505 0.5574258   651  val
 7:       Morgan -0.08364600 0.25807570 0.7281817 0.5574218   534  val
 8:       Mannum -1.03358626 0.09670613 0.6778615 0.5275489   319  val
 9: MurrayBridge -1.81499914 0.04647636 0.6463762 0.5202518   189  val
10:   TailemBend -0.56328860 0.07092222 0.8611857 0.6353792   305  val

$`yd7, Temperature, Salinity, Velocity, Discharge`
            site        NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.7037685 0.74452672 0.4212865 0.3089039   651  cal
 2:       Morgan  0.6559181 0.71741061 0.4103240 0.2919967   534  cal
 3:       Mannum  0.3475171 0.62114438 0.3839673 0.2828659   319  cal
 4: MurrayBridge  0.1837637 0.54691645 0.3480600 0.2620497   189  cal
 5:   TailemBend  0.3672259 0.54508671 0.5479004 0.3721077   305  cal
 6:        Lock9  0.1237166 0.28970997 0.7245764 0.5682562   651  val
 7:       Morgan -0.1480072 0.23705309 0.7494944 0.5779731   534  val
 8:       Mannum -1.0693230 0.08659082 0.6837917 0.5312485   319  val
 9: MurrayBridge -1.8894685 0.04575365 0.6548701 0.5290008   189  val
10:   TailemBend -0.6119799 0.08209962 0.8744944 0.6538793   305  val
```

### 1.2 Radiation  
Sadly, using `Radiation`, `NSE` is significantly decreased.


```
$`yd7, Temperature, Salinity, Velocity`
            site         NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.70150837 0.73115649 0.4228905 0.3121879   651  cal
 2:       Morgan  0.63005957 0.70529995 0.4254632 0.3010108   534  cal
 3:       Mannum  0.33624239 0.62099406 0.3872705 0.2831688   319  cal
 4: MurrayBridge -0.09746892 0.39714104 0.4035918 0.2841660   189  cal
 5:   TailemBend  0.25941595 0.45781335 0.5927402 0.4031068   305  cal
 6:        Lock9  0.17369330 0.32447272 0.7036109 0.5533502   651  val
 7:       Morgan -0.06936289 0.26481075 0.7233669 0.5563805   534  val
 8:       Mannum -1.06302923 0.09044687 0.6827511 0.5320004   319  val
 9: MurrayBridge -1.80725772 0.05237189 0.6454868 0.5196895   189  val
10:   TailemBend -0.54469333 0.07733961 0.8560485 0.6309572   305  val

$`yd7, TempWater, Salinity, Velocity`
            site         NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.71570459 0.75253206 0.4126925 0.3060618   655  cal
 2:       Morgan  0.57980618 0.66997988 0.4534411 0.3156416   534  cal
 3:       Mannum  0.32955032 0.60704459 0.3892627 0.2868653   322  cal
 4: MurrayBridge -0.16542148 0.38042572 0.4148385 0.2978823   190  cal
 5:   TailemBend  0.40503603 0.58279245 0.5282612 0.3609182   310  cal
 6:        Lock9  0.15997606 0.32396047 0.7093940 0.5520487   655  val
 7:       Morgan -0.04583989 0.29049306 0.7153666 0.5377523   534  val
 8:       Mannum -1.09202937 0.09418040 0.6876122 0.5354767   322  val
 9: MurrayBridge -1.89788369 0.02544118 0.6541511 0.5163799   190  val
10:   TailemBend -0.63333400 0.06862169 0.8752677 0.6547089   310  val

$`yd7, Temperature, Salinity, Velocity, Radiation`
            site        NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.6461378 0.70154725 0.4604463 0.3306500   651  cal
 2:       Morgan  0.5658795 0.67247774 0.4608942 0.3168438   534  cal
 3:       Mannum  0.2599561 0.55906075 0.4089201 0.2976189   319  cal
 4: MurrayBridge -0.1910959 0.39363144 0.4204550 0.3049601   189  cal
 5:   TailemBend  0.3301252 0.54073881 0.5637338 0.3888636   305  cal
 6:        Lock9  0.1290707 0.29338160 0.7223594 0.5567326   651  val
 7:       Morgan -0.1131159 0.24767987 0.7380168 0.5628780   534  val
 8:       Mannum -1.0820829 0.08159273 0.6858967 0.5342710   319  val
 9: MurrayBridge -1.8357280 0.05141600 0.6487517 0.5231566   189  val
10:   TailemBend -0.6073181 0.06800371 0.8732290 0.6411703   305  val
```

## 2. Temperature response function  


```
$`yd7, Temperature, Salinity, Velocity`
            site         NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.72647874 0.75511491 0.4048158 0.3012344   651  cal
 2:       Morgan  0.63005957 0.70529995 0.4254632 0.3010108   534  cal
 3:       Mannum  0.33624239 0.62099406 0.3872705 0.2831688   319  cal
 4: MurrayBridge -0.09746892 0.39714104 0.4035918 0.2841660   189  cal
 5:   TailemBend  0.25941595 0.45781335 0.5927402 0.4031068   305  cal
 6:        Lock9  0.17369330 0.32447272 0.7036109 0.5533502   651  val
 7:       Morgan -0.06936289 0.26481075 0.7233669 0.5563805   534  val
 8:       Mannum -1.06302923 0.09044687 0.6827511 0.5320004   319  val
 9: MurrayBridge -1.80725772 0.05237189 0.6454868 0.5196895   189  val
10:   TailemBend -0.54469333 0.07733961 0.8560485 0.6309572   305  val

$`yd7, TempWater, Salinity, Velocity`
            site         NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.71570459 0.75253206 0.4126925 0.3060618   655  cal
 2:       Morgan  0.57980618 0.66997988 0.4534411 0.3156416   534  cal
 3:       Mannum  0.32955032 0.60704459 0.3892627 0.2868653   322  cal
 4: MurrayBridge -0.16542148 0.38042572 0.4148385 0.2978823   190  cal
 5:   TailemBend  0.40503603 0.58279245 0.5282612 0.3609182   310  cal
 6:        Lock9  0.15997606 0.32396047 0.7093940 0.5520487   655  val
 7:       Morgan -0.04583989 0.29049306 0.7153666 0.5377523   534  val
 8:       Mannum -1.09202937 0.09418040 0.6876122 0.5354767   322  val
 9: MurrayBridge -1.89788369 0.02544118 0.6541511 0.5163799   190  val
10:   TailemBend -0.63333400 0.06862169 0.8752677 0.6547089   310  val

$`yd7, Temperature, Salinity, Velocity, PTwater`
            site         NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.66107622 0.71100366 0.4506225 0.3264652   651  cal
 2:       Morgan  0.59922630 0.68369893 0.4428388 0.3204444   534  cal
 3:       Mannum  0.21583580 0.56263443 0.4209332 0.3093786   319  cal
 4: MurrayBridge -0.04667773 0.49126449 0.3941420 0.2972076   189  cal
 5:   TailemBend  0.42549700 0.58925350 0.5220636 0.3744973   305  cal
 6:        Lock9  0.13109173 0.29189519 0.7215208 0.5613938   651  val
 7:       Morgan -0.13437007 0.23259860 0.7450295 0.5731687   534  val
 8:       Mannum -1.13835811 0.07513736 0.6951042 0.5401650   319  val
 9: MurrayBridge -1.97064256 0.02962386 0.6640051 0.5288655   189  val
10:   TailemBend -0.61749728 0.06313018 0.8759897 0.6426116   305  val

$`yd7, TempWater, Salinity, Velocity, PTwater2`
            site         NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.68971240 0.74334842 0.4311455 0.3142905   655  cal
 2:       Morgan  0.62169566 0.71208945 0.4302459 0.3142160   534  cal
 3:       Mannum  0.36523113 0.64024612 0.3787630 0.2848064   322  cal
 4: MurrayBridge  0.02991621 0.52713889 0.3784794 0.2849408   190  cal
 5:   TailemBend  0.49756664 0.67989724 0.4854479 0.3503125   310  cal
 6:        Lock9  0.12976053 0.30044547 0.7220397 0.5570191   655  val
 7:       Morgan -0.11022032 0.24834161 0.7370563 0.5565513   534  val
 8:       Mannum -1.20158227 0.06547755 0.7053865 0.5493963   322  val
 9: MurrayBridge -2.02797463 0.02235937 0.6686729 0.5296739   190  val
10:   TailemBend -0.73326943 0.04866619 0.9016467 0.6785899   310  val

$`yd7, Salinity, Velocity, PTwater`
            site         NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.69416795 0.73983980 0.4280588 0.3110602   651  cal
 2:       Morgan  0.62593265 0.70674459 0.4278297 0.3078247   534  cal
 3:       Mannum  0.32253114 0.59276456 0.3912500 0.2862252   319  cal
 4: MurrayBridge -0.30523533 0.37229217 0.4401398 0.3169142   189  cal
 5:   TailemBend  0.31810135 0.49204780 0.5687706 0.3832915   305  cal
 6:        Lock9  0.15055732 0.30801914 0.7133931 0.5604530   651  val
 7:       Morgan -0.07193441 0.25857894 0.7242361 0.5564414   534  val
 8:       Mannum -1.06960287 0.09292106 0.6838380 0.5318553   319  val
 9: MurrayBridge -1.84849504 0.03397577 0.6502104 0.5171845   189  val
10:   TailemBend -0.56228056 0.07790888 0.8609080 0.6307583   305  val

$`yd7, Salinity, Velocity, PTwater2`
            site         NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.70012354 0.73685002 0.4238506 0.3161926   655  cal
 2:       Morgan  0.59384477 0.68655355 0.4458021 0.3089404   534  cal
 3:       Mannum  0.25136026 0.56263015 0.4113355 0.2956088   322  cal
 4: MurrayBridge  0.07415034 0.48390078 0.3697497 0.2707491   190  cal
 5:   TailemBend  0.33813291 0.50588197 0.5571713 0.3841787   310  cal
 6:        Lock9  0.14899517 0.31557192 0.7140156 0.5534213   655  val
 7:       Morgan -0.05501392 0.28130638 0.7184973 0.5405797   534  val
 8:       Mannum -1.10212107 0.08595810 0.6892687 0.5319263   322  val
 9: MurrayBridge -1.95684800 0.02413943 0.6607727 0.5197973   190  val
10:   TailemBend -0.63633316 0.07353641 0.8760709 0.6560984   310  val
```

## 3. Growth rate   

If using `(log(C(t)) - log(C(t-1)) )/dt`, 1110(24.2%) observations are missing.


```r
# according result, `velocity` is more important
predictors_list <- list(
    c("yd7", "Temperature", "Salinity", "Velocity"),                      
    c("yd7", "TempWater", "Salinity", "Velocity"),
    c("yd7", "Temperature", "Salinity", "Velocity", "Radiation")
    # c("yd7", "Temperature", "Salinity", "Discharge"),
    # c("yd7", "Temperature", "Salinity", "Velocity", "Discharge")
) %>% set_names(map(., paste, collapse = ", "))

info_lst1 <- llply(predictors_list, RF_gof, response = "CYA_sum1")
print(info_lst1)
```

```
$`yd7, Temperature, Salinity, Velocity`
            site         NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.67638852 0.71268946 0.4438813 0.3156921   558  cal
 2:       Morgan  0.61675058 0.68944232 0.4384758 0.3067482   492  cal
 3:       Mannum  0.38185306 0.61028226 0.3866941 0.2709090   259  cal
 4: MurrayBridge  0.12524935 0.44589921 0.3629343 0.2567247   149  cal
 5:   TailemBend  0.42909036 0.57131571 0.5492982 0.3772103   236  cal
 6:        Lock9  0.13086939 0.27492992 0.7274404 0.5629103   558  val
 7:       Morgan -0.02416521 0.26346652 0.7167866 0.5427318   492  val
 8:       Mannum -0.79183407 0.17160516 0.6583708 0.5124917   259  val
 9: MurrayBridge -1.26459496 0.06213952 0.5839574 0.4695097   149  val
10:   TailemBend -0.51338845 0.05423274 0.8943339 0.6658231   236  val

$`yd7, TempWater, Salinity, Velocity`
            site        NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.6563660 0.69427666 0.4569774 0.3258841   561  cal
 2:       Morgan  0.6090709 0.67736289 0.4428472 0.3091320   492  cal
 3:       Mannum  0.4800402 0.67971284 0.3546386 0.2659401   261  cal
 4: MurrayBridge  0.3453735 0.61060070 0.3139660 0.2304200   149  cal
 5:   TailemBend  0.4467063 0.58580210 0.5377249 0.3750803   239  cal
 6:        Lock9  0.1353698 0.28872382 0.7248729 0.5619807   561  val
 7:       Morgan -0.1027410 0.21873464 0.7437751 0.5776475   492  val
 8:       Mannum -0.8406771 0.12669285 0.6672521 0.5153190   261  val
 9: MurrayBridge -1.3927279 0.01678845 0.6002505 0.4722534   149  val
10:   TailemBend -0.3889825 0.09866822 0.8519826 0.6287178   239  val

$`yd7, Temperature, Salinity, Velocity, Radiation`
            site         NSE         R2      RMSE       MAE n_sim type
 1:        Lock9  0.70089653 0.74077573 0.4267422 0.3141071   558  cal
 2:       Morgan  0.64088825 0.69982983 0.4244433 0.3022024   492  cal
 3:       Mannum  0.46925044 0.69196947 0.3583163 0.2694481   259  cal
 4: MurrayBridge  0.29985137 0.56636318 0.3246990 0.2357574   149  cal
 5:   TailemBend  0.51759777 0.64887871 0.5049276 0.3563161   236  cal
 6:        Lock9  0.13390400 0.27453623 0.7261693 0.5553594   558  val
 7:       Morgan -0.07439163 0.23464542 0.7341524 0.5514405   492  val
 8:       Mannum -0.82890204 0.15618160 0.6651459 0.5239452   259  val
 9: MurrayBridge -1.25161399 0.07408345 0.5822813 0.4694752   149  val
10:   TailemBend -0.56230640 0.03950430 0.9086729 0.6681741   236  val
```
