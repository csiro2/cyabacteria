# Predicting cyanobacteria using regression tree ensemble


> Dongdong Kong  
> ------
    
## 2019-05-17

> **Q1**: 我看看你代码里select variables后面的一些例子。前五行是五个站点的calibration结果，后面五行是validation结果吧，好像效果都不是很好。但是Klaus说你们有好一点的结果“We managed to get the NSE much better with different drivers, i.e. upstream cyano biomass and previous value at same location, plus other drivers as solar radiation, water temperature etc.” 这是哪个结果？

**REPLY:** 这个代码在: [test/Figure4_rf_result.R](test/Figure4_rf_result.R)。考虑自相关和上游的站点的影响，可以微弱提升`RF`表现。



>  **Q2**: 另外这应该是个时序预测模型吧？你们时序是怎么处理的？有没有用一段时间的时序数据来训练，另外一段时间的时序数据来测试？

**REPLY:** (1) 时间序列处理详见(test/data_01_tidy_cyaBacteria.R, data_02_rf_INPUT.R, Figure4_rf_result (prepare input).R);  (2) 模拟分为calibration和5-fold cross validation, 详见[R/perdict_bacteria.R](R/perdict_bacteria.R)



>  **Q3**: 另外随机森林算法工具包中一般会带有特征重要性评估的方法(基尼系数、增益率等)，为什么不用这个，而去增加或减少特征来比较指标性能的方法去评估特征重要性呢？

**REPLY:** 这两个指标应该是在randomForest返回值中的importance变量，之前也注意到了。采用importance指标，或 NSE of cross validation几乎没有差别。而模型的目的是为了提升模拟精度，因此就只选用了NSE来挑选变量。



> **Q4**: Klaus想看看“I also wonder how the validation would look like if we exclude the fit to the large blooms, or not calculating for the entire time series but restrict to seasons.”如何修改代码进行测试呢？

**REPLY:** 这个代码在: [test/Figure4_rf_result.R](test/Figure4_rf_result.R)，修改L12, 和L43即可。<u>*注意predicting时的输入数据*</u>。


## 2019-05-19

> **Q1**: res_v1中info计算的指标是calibration的RMSE,NSE等结果？五折cross-validation的指标有计算么？

`info` 已经包含了calibration (`ySim`)和validation(`yVal`)的结果。


> **Q2**: 另外要exclude the fit to the large blooms，你说修改density_min = 2 # minimum value of bacteria density used in RF，这个density要怎样限制呢？还是我需要问Klaus?

是的，您需要问一下Klaus. 之前我做的是density_min取2，也跟Klaus汇报过。模拟结果确实会提高一些。
