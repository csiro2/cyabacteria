## R version 

1. Install packages first

```R
install.packages(c('Matrix', 'data.table', 'tidyverse', 'plotly', 'devtools', 
        'broom', 'lubridate', 'devtools', 'Cairo' 
        'zoo', 'randomForest', 'caret'))
devtools::install_github('kongdd/Ipaper')
devtools::install_github('kongdd/plyr')
devtools::install_github('kongdd/phenofit')
```
2. Run main function `main_cyanobacteria.Rmd`

