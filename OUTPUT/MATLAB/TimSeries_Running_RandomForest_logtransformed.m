%% YQZ, CLW, 24 July 2018
% Logtransformed, to avoid 0 value issues, all CYA counts added 1.
% Four modelling experiments:
% 1. -	Temp only
% 2. -	Velocity and temp
% 3. -	Velocity,temp and EC
% 4. -  Discharge, velocity, temp and EC

clear;clc;close all;


%% environment setup

OUT = '\\osm-27-cdc.it.csiro.au\OSM_CBR_LW_HYDROFLUSH_work\Goyder\Cyanobacteria\Processed\';

%% putting together
% Selected_FGs = {'CHL','CYA','CYA_I','CYA_IIIb','CYA_IIIp','CYA_IVb','CYA_IVp'};
Selected_FGs = {'CYA_I','CYA_IIIb','CYA_IIIp','CYA_IVb','CYA_IVp','CYA'};

Selected_sites = {'River Murray Morgan Sample Pump','River Murray Mannum 2 sites','River Murray Murray Bridge 2 sites',...
    'River Murray Tailem Bend 2 sites','River Murray Lock 9'};




%% Four modelling experiments:
% 1. -	Temp only
% 2. -	Velocity and temp
% 3. -	Velocity,temp and EC
% 4. -  Vol, Velocity, temp and EC
% Start_num = [6 5 5 4];
% End_num   = [6 6 7 7];
% Exps      = {'Temp','Temp_velocity','Temp_velocity_EC','Temp_Discharge_velocity_EC'};
Start_num = [6 5 5];
End_num   = [6 6 7];
Exps      = {'Temp','Temp_velocity','Temp_velocity_EC'};
xxx       = 3;


%% Calibrations or training

load([OUT 'RandomForest_training_' Exps{xxx} ], 'NSE','KGE','SampleN','r','Selected_FGs','Selected_sites','Dates','ResponseV_Matrix');


OUTPUTS_MATRIX = [Dates ResponseV_Matrix{5,6}];
%% k-fold cross validation: 20-fold

load([OUT 'RandomForest_crossvalidation_20fold_' Exps{xxx}], 'NSE','KGE','SampleN','r','Selected_FGs','Selected_sites','Dates','ResponseV_Matrix');

OUTPUTS_MATRIX      = [OUTPUTS_MATRIX ResponseV_Matrix{5,6}];
OUTPUTS_MATRIX(:,6) = [];
DATENUMS = datenum(OUTPUTS_MATRIX(:,1),OUTPUTS_MATRIX(:,2),OUTPUTS_MATRIX(:,3));

figure(1)
set(gcf,'pos',[499,376,1725,819]);

plot(DATENUMS,OUTPUTS_MATRIX(:,4),'k');
hold on;
plot(DATENUMS,OUTPUTS_MATRIX(:,5),'b','LineWidth',2);
plot(DATENUMS,OUTPUTS_MATRIX(:,6),'r','LineWidth',2);

datetick('x',10)
xlabel('Year');
xlim([datenum(1994,1,1) datenum(2018,2,28)]);
ylabel('Total CYA (cells/ml, log-transformed)')
h=legend('Observed','Training','20-fold cross validation');
set(h,'FontSize',16);
set(gca,'FontSize',16);
grid on;

print ([OUT 'Plotting\Time_Series_Lock 9_' Exps{xxx}], '-dpng','-r300');

