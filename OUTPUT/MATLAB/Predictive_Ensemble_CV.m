%% YQZ, CLW, 19 July 2018

clear;clc;close all;
load_pkgs;
%%

for ii=1:length(Selected_sites)
    
    fprintf('ii=%.0d\n',ii);
    [INPUTS, INPUTS_NAMES] = xlsread([OUT Selected_sites{ii} '_Pred_Dep.xls'],'outputs');
    
    for jj=1:length(Selected_FGs)
        
        YX         = INPUTS(:,[jj+7, 4:7 ]);
        NUMS       = (isnan(YX(:,1))==1);
        YX(NUMS,:) = [];
        
        Discharge  = YX(:,2);
        Velocity   = YX(:,3);
        Temperature= YX(:,4);
        EC         = YX(:,5);
        
        ResponseV  = YX(:,1);
        
        Tbl                     = table(Velocity,Temperature,EC,ResponseV);
        [MdlFinal, minErr]      = PredictiveEnsemble(Tbl,'ResponseV');
        ResponseV_Matrix{ii,jj} = MdlFinal;
        min_MSE_Matrix {ii,jj}  = minErr;
        
    end
    
end

% save([OUT 'PredictiveEnsemble_CrossValidation'], 'ResponseV_Matrix','min_MSE_Matrix','Selected*');

