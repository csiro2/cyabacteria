%% YQZ, CLW, 23 July 2018
% do sum when there are multiple sample in the same day
% If you would try your algorithm now, using the 8 groups:
% DIA, CHL, CYA_I, CYA_IIIb, CYA_IIIp, Cya_IVb, CYA_IVp,
% and the sum of all (CYA, CYA_I, CYA_IIIb, CYA_IIIp, CYA_IVb, CYA_IVp).
% Restricting everything to the time after 1994-01-21.

%% YQZ, CLW, 18 July 2018

clear;clc;close all;


%% environment setup

OUT = '\\osm-27-cdc.it.csiro.au\OSM_CBR_LW_HYDROFLUSH_work\Goyder\Cyanobacteria\Processed\';
INPUTS = '\\osm-27-cdc.it.csiro.au\OSM_CBR_LW_HYDROFLUSH_work\Goyder\Cyanobacteria\From_Matt_20180416\';


%% calculating dd mm yyyy

% [Forcings,Forcing_names] = xlsread([INPUTS 'CyanobacteriaInputs.xlsx'],'DailyData');
[Forcings,Forcing_names] = xlsread([OUT 'CyanobacteriaInputs_gapFilled_level0.xlsx'],'DailyData');


%% Get five sites hydrological inputs dataset
[CYA,CYA_names] = xlsread([OUT 'CYA_CHL_FiveSites.xls'],'outputs');
OUT = '\';
%% putting together
Selected_FGs = {'DIA','CHL','CYA','CYA_I','CYA_IIIb','CYA_IIIp','CYA_IVb','CYA_IVp'};

Selected_sites = {'River Murray Morgan Sample Pump','River Murray Mannum 2 sites','River Murray Murray Bridge 2 sites',...
    'River Murray Tailem Bend 2 sites','River Murray Lock 9'};

% DATENUMS_forcing = [datenum(1970,1,1):datenum(2018,2,12)]';
DATENUMS_forcing = [datenum(1994,1,21):datenum(2018,2,12)]';
DATESTR = datestr(DATENUMS_forcing,24);
YEAR    = str2num(DATESTR(:,7:10));
MONTH   = str2num(DATESTR(:,4:5));
DAY     = str2num(DATESTR(:,1:2));

for ii=1:length(CYA)
    TEMP = CYA_names{ii+1,6};
    if length(TEMP)==10
        DATENUMS_CYA(ii,1) = datenum(str2num(TEMP(7:10)),str2num(TEMP(4:5)),str2num(TEMP(1:2)));
    else
        DATENUMS_CYA(ii,1) = datenum(str2num(TEMP(6:9)),str2num(TEMP(3:4)),str2num(TEMP(1)));
        
    end
end

%% output predictors and dependant variables
for ii = 1:length(Selected_sites)
    
    fprintf('ii=%.0d\n',ii);
    TEMP   = Forcings(:,(ii-1)*4+1:ii*4);
    
    for kk=1:length(Selected_FGs)
        for jj= 1:length(CYA)
            if strcmp(CYA_names{jj+1,5},Selected_sites{ii})==1 & strcmp(CYA_names{jj+1,11},Selected_FGs{kk})==1
                YES_No(jj,1)=1;
            else
                YES_No(jj,1)=0;
            end
        end
        
        NUMS                     = find(YES_No==0);
        CYA_TEMP                 = CYA;
        CYA_TEMP(NUMS,:)         = [];
        DATENUMS_CYA_TEMP        = DATENUMS_CYA;
        DATENUMS_CYA_TEMP(NUMS)  = [];
        CYA_names_TEMP           = CYA_names;
        CYA_names_TEMP(NUMS+1,:) = [];
        
        for jj=1:length(DATENUMS_forcing)
            
            NUM = find(DATENUMS_CYA_TEMP == DATENUMS_forcing(jj));
            if length(NUM)>=1
                Dependants(jj,kk)= sum(CYA_TEMP(NUM,7));
            else
                Dependants(jj,kk)=nan;
            end
        end
        
    end
    
    OUTPUTS       = [YEAR MONTH DAY TEMP Dependants];
    for xyz = 1:size(OUTPUTS,1)
        if length(find(isnan(OUTPUTS(xyz,10:15))==1)) <6
            TEMPS(xyz,1) = nansum(OUTPUTS(xyz,10:15),2);
        else
            TEMPS(xyz,1) = nan;
        end
    end
    
    OUTPUTS = [OUTPUTS TEMPS]; clear TEMPS;
    OUTPUTS_names = {'Year','Month','Day','Discharge (ML/d)','Velocity (m/s)', 'Temperature (oC)','EC','DIA','CHL','CYA','CYA_I','CYA_IIIb','CYA_IIIp','CYA_IVb','CYA_IVp','CYA (sum'};
    
    xlswrite([OUT Selected_sites{ii} '_Pred_Dep.xls'],OUTPUTS,'outputs','a2');
    xlswrite([OUT Selected_sites{ii} '_Pred_Dep.xls'],OUTPUTS_names,'outputs','a1');
    
    clear Dependants YES_No;
    
end
% for jj=1:size(TEMP,1)
%
%
% end