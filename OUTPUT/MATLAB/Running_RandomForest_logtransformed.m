%% YQZ, CLW, 24 July 2018
% Logtransformed, to avoid 0 value issues, all CYA counts added 1.
% Four modelling experiments:
% 1. -	Temp only
% 2. -	Velocity and temp
% 3. -	Velocity,temp and EC
% 4. -  Discharge, velocity, temp and EC

clear;clc;close all;
load_pkgs;
%%
% Selected_FGs = {'CHL','CYA','CYA_I','CYA_IIIb','CYA_IIIp','CYA_IVb','CYA_IVp'};
Selected_FGs = {'CYA_I','CYA_IIIb','CYA_IIIp','CYA_IVb','CYA_IVp','CYA'};
OUT2 = 'OUTPUT/';

%% Four modelling experiments:
% 1. -	Temp only
% 2. -	Velocity and temp
% 3. -	Velocity,temp and EC
% 4. -  Vol, Velocity, temp and EC
% Start_num = [6 5 5 4];
% End_num   = [6 6 7 7];
% Exps      = {'Temp','Temp_velocity','Temp_velocity_EC','Temp_Discharge_velocity_EC'};
Start_num = [6 5 5];
End_num   = [6 6 7];
Exps      = {'Temp','Temp_velocity','Temp_velocity_EC'};

CYA_start_Col = 10;

%% Calibrations or training
% for kk=1:length(Start_num)
%     figure(1)
%     set(gcf,'pos',[32, 49,2316,1308]);
%     
%     for ii=1:length(Selected_sites)
%         
%         fprintf('ii=%.0d\n',ii);
%         [INPUTS, INPUTS_NAMES] = xlsread([OUT Selected_sites{ii} '_Pred_Dep.xls'],'outputs');
%         
%         for jj=1:length(Selected_FGs)
%             YX         = [log(INPUTS(:,jj+CYA_start_Col)+1) INPUTS(:,[Start_num(kk):End_num(kk)])];
%             NUMS       = (isnan(YX(:,1))==1);
%             Dates      = INPUTS(:,1:3); Dates(NUMS,:) = [];
%             YX(NUMS,:) = [];
%             %             if size(YX,1)>10
%             ResponseV         = RandomForest(YX);
%             NSE(ii,jj)        = CE(ResponseV);
%             KGE(ii,jj)        = klinggupta(ResponseV(:,2), ResponseV(:,1));
%             TEMP              = corrcoef(ResponseV);
%             r(ii,jj)          = TEMP(2,1);
%             % plotting
%             %         figure(1)
%             subplot(length(Selected_sites),length(Selected_FGs),(ii-1)*length(Selected_FGs)+jj)
%             plot(ResponseV(:,1),ResponseV(:,2),'r.','MarkerSize',8);
%             xlim([0 max(ResponseV(:,1))*1.05]);
%             ylim([0 max(ResponseV(:,1))*1.05]);
%             hold on;
%             plot([0 max(ResponseV(:,1))*1.05],[0 max(ResponseV(:,1))*1.05],'k');
%             text(max(ResponseV(:,1))*0.1, max(ResponseV(:,1))*0.95,sprintf('NSE = %.2f',NSE(ii,jj)),'Fontsize',16);
%             NUMS = length(find(isnan(ResponseV(:,1))==0 & isnan(ResponseV(:,2))==0));
%             SampleN(ii,jj)    = NUMS;
%             
%             text(max(ResponseV(:,1))*0.1, max(ResponseV(:,1))*0.80,sprintf('N = %.0d',NUMS),'Fontsize',16);
%             
%             ResponseV_Matrix{ii,jj} = ResponseV;
%             %             end
%         end
%         
%     end
%     
%     save([OUT2 'RandomForest_training_' Exps{kk} ], 'NSE','KGE','SampleN','r','Selected_FGs','Selected_sites','Dates','ResponseV_Matrix');
%     print ([OUT2 'Plotting\RandomForest_training_' Exps{kk}], '-dpng','-r300');
%     close;
% end

%% k-fold cross validation: 5-fold, 10-fold
kfold = 5;

% close; figure(1)
% set(gcf,'pos',[32, 49,2316,1308]);
for xxx=1:length(Start_num)
    figure(1)
    set(gcf,'pos',[32, 49,2316,1308]);
    
    for ii=1:length(Selected_sites)
        
        fprintf('ii=%.0d\n',ii);
        [INPUTS, INPUTS_NAMES] = xlsread([OUT Selected_sites{ii} '_Pred_Dep.xls'],'outputs');
        
        for jj=1:length(Selected_FGs)
            
            YX         = [log(INPUTS(:,jj+CYA_start_Col)+1) INPUTS(:,[Start_num(xxx):End_num(xxx)])];
            %             YX         = [log(INPUTS(:,jj+7)+1) INPUTS(:,[5:6])];
            %             YX         = INPUTS(:,[jj+7, 5:6]);
            %         NUMS       = (isnan(YX(:,1))==1 | isnan(YX(:,2))==1 | isnan(YX(:,3))==1 | isnan(YX(:,4))==1);
            NUMS       = (isnan(YX(:,1))==1);
            Dates      = INPUTS(:,1:3); Dates(NUMS,:) = [];
            YX(NUMS,:) = [];
            
            ResponseV = RandomForest(YX, kfold); % ResponseV = [Y ySim yPred];
            ResponseV = ResponseV(:, [1, 3]);

            %             if exist('ResponseV')~=0
            %             SampleN(ii,jj)    = size(ResponseV,1);
            NUMS = length(find(isnan(ResponseV(:,1))==0 & isnan(ResponseV(:,2))==0));
            SampleN(ii,jj)    = NUMS;
            NSE(ii,jj)        = CE(ResponseV);
            KGE(ii,jj)        = klinggupta(ResponseV(:,2), ResponseV(:,1));
            TEMP              = corrcoef(ResponseV);
            r(ii,jj)          = TEMP(2,1);
            
            % plotting
            %         figure(1)
            subplot(length(Selected_sites),length(Selected_FGs),(ii-1)*length(Selected_FGs)+jj)
            plot(ResponseV(:,1),ResponseV(:,2),'r.','MarkerSize',8);
            xlim([0 max(ResponseV(:,1))*1.05]);
            ylim([0 max(ResponseV(:,1))*1.05]);
            hold on;
            plot([0 max(ResponseV(:,1))*1.05],[0 max(ResponseV(:,1))*1.05],'k');
            text(max(ResponseV(:,1))*0.1, max(ResponseV(:,1))*0.95,sprintf('NSE = %.2f',NSE(ii,jj)),'Fontsize',16);
            text(max(ResponseV(:,1))*0.1, max(ResponseV(:,1))*0.80,sprintf('N = %.0d',NUMS),'Fontsize',16);
            
            ResponseV_Matrix{ii,jj} = ResponseV;
            clear ResponseV Folder_kfold_S Folder_kfold_E;
            %             end
        end
        
    end
    
    
    save([OUT2 'RandomForest_crossvalidation_5fold_' Exps{xxx}], 'NSE','KGE','SampleN','r','Selected_FGs','Selected_sites','Dates','ResponseV_Matrix');
    print ([OUT2 'Plotting\RandomForest_crossvalidation_5fold_' Exps{xxx}], '-dpng','-r300');
    close;
end



%% k-fold cross validation: 10-fold
kfold = 10;

% close; figure(1)
% set(gcf,'pos',[32, 49,2316,1308]);
for xxx=1:length(Start_num)
    figure(1)
    set(gcf,'pos',[32, 49,2316,1308]);
    
    for ii=1:length(Selected_sites)
        
        fprintf('ii=%.0d\n',ii);
        [INPUTS, INPUTS_NAMES] = xlsread([OUT Selected_sites{ii} '_Pred_Dep.xls'],'outputs');
        
        for jj=1:length(Selected_FGs)
            
            YX         = [log(INPUTS(:,jj+CYA_start_Col)+1) INPUTS(:,[Start_num(xxx):End_num(xxx)])];
            %             YX         = [log(INPUTS(:,jj+7)+1) INPUTS(:,[5:6])];
            %             YX         = INPUTS(:,[jj+7, 5:6]);
            %         NUMS       = (isnan(YX(:,1))==1 | isnan(YX(:,2))==1 | isnan(YX(:,3))==1 | isnan(YX(:,4))==1);
            NUMS       = (isnan(YX(:,1))==1);
            Dates      = INPUTS(:,1:3); Dates(NUMS,:) = [];
            YX(NUMS,:) = [];
            
            ResponseV = RandomForest(YX, kfold); % ResponseV = [Y ySim yPred];
            ResponseV = ResponseV(:, [1, 3]);
            
            %             if exist('ResponseV')~=0
            %             SampleN(ii,jj)    = size(ResponseV,1);
            NUMS = length(find(isnan(ResponseV(:,1))==0 & isnan(ResponseV(:,2))==0));
            SampleN(ii,jj)    = NUMS;
            NSE(ii,jj)        = CE(ResponseV);
            KGE(ii,jj)        = klinggupta(ResponseV(:,2), ResponseV(:,1));
            TEMP              = corrcoef(ResponseV);
            r(ii,jj)          = TEMP(2,1);
            
            % plotting
            %         figure(1)
            subplot(length(Selected_sites),length(Selected_FGs),(ii-1)*length(Selected_FGs)+jj)
            plot(ResponseV(:,1),ResponseV(:,2),'r.','MarkerSize',8);
            xlim([0 max(ResponseV(:,1))*1.05]);
            ylim([0 max(ResponseV(:,1))*1.05]);
            hold on;
            plot([0 max(ResponseV(:,1))*1.05],[0 max(ResponseV(:,1))*1.05],'k');
            text(max(ResponseV(:,1))*0.1, max(ResponseV(:,1))*0.95,sprintf('NSE = %.2f',NSE(ii,jj)),'Fontsize',16);
            text(max(ResponseV(:,1))*0.1, max(ResponseV(:,1))*0.80,sprintf('N = %.0d',NUMS),'Fontsize',16);
            
            ResponseV_Matrix{ii,jj} = ResponseV;
            clear ResponseV Folder_kfold_S Folder_kfold_E;
            %             end
        end
        
    end
    
    
    save([OUT2 'RandomForest_crossvalidation_10fold_' Exps{xxx}], 'NSE','KGE','SampleN','r','Selected_FGs','Selected_sites','Dates','ResponseV_Matrix');
    print ([OUT2 'Plotting\RandomForest_crossvalidation_10fold_' Exps{xxx}], '-dpng','-r300');
    close;
end


%% k-fold cross validation: 20-fold
kfold = 20;

% close; figure(1)
% set(gcf,'pos',[32, 49,2316,1308]);
for xxx=1:length(Start_num)
    figure(1)
    set(gcf,'pos',[32, 49,2316,1308]);
    
    for ii=1:length(Selected_sites)
        
        fprintf('ii=%.0d\n',ii);
        [INPUTS, INPUTS_NAMES] = xlsread([OUT Selected_sites{ii} '_Pred_Dep.xls'],'outputs');
        
        for jj=1:length(Selected_FGs)
            
            YX         = [log(INPUTS(:,jj+CYA_start_Col)+1) INPUTS(:,[Start_num(xxx):End_num(xxx)])];
            %             YX         = [log(INPUTS(:,jj+7)+1) INPUTS(:,[5:6])];
            %             YX         = INPUTS(:,[jj+7, 5:6]);
            %         NUMS       = (isnan(YX(:,1))==1 | isnan(YX(:,2))==1 | isnan(YX(:,3))==1 | isnan(YX(:,4))==1);
            NUMS       = (isnan(YX(:,1))==1);
            Dates      = INPUTS(:,1:3); Dates(NUMS,:) = [];
            YX(NUMS,:) = [];
            
            ResponseV = RandomForest(YX, kfold); % ResponseV = [Y ySim yPred];
            ResponseV = ResponseV(:, [1, 3]);
            
            %             if exist('ResponseV')~=0
            %             SampleN(ii,jj)    = size(ResponseV,1);
            NUMS = length(find(isnan(ResponseV(:,1))==0 & isnan(ResponseV(:,2))==0));
            SampleN(ii,jj)    = NUMS;
            NSE(ii,jj)        = CE(ResponseV);
            KGE(ii,jj)        = klinggupta(ResponseV(:,2), ResponseV(:,1));
            TEMP              = corrcoef(ResponseV);
            r(ii,jj)          = TEMP(2,1);
            
            % plotting
            %         figure(1)
            subplot(length(Selected_sites),length(Selected_FGs),(ii-1)*length(Selected_FGs)+jj)
            plot(ResponseV(:,1),ResponseV(:,2),'r.','MarkerSize',8);
            xlim([0 max(ResponseV(:,1))*1.05]);
            ylim([0 max(ResponseV(:,1))*1.05]);
            hold on;
            plot([0 max(ResponseV(:,1))*1.05],[0 max(ResponseV(:,1))*1.05],'k');
            text(max(ResponseV(:,1))*0.1, max(ResponseV(:,1))*0.95,sprintf('NSE = %.2f',NSE(ii,jj)),'Fontsize',16);
            text(max(ResponseV(:,1))*0.1, max(ResponseV(:,1))*0.80,sprintf('N = %.0d',NUMS),'Fontsize',16);
            
            ResponseV_Matrix{ii,jj} = ResponseV;
            clear ResponseV Folder_kfold_S Folder_kfold_E;
            %             end
        end
        
    end
    
    save([OUT2 'RandomForest_crossvalidation_20fold_' Exps{xxx}], 'NSE','KGE','SampleN','r','Selected_FGs','Selected_sites','Dates','ResponseV_Matrix');
    print ([OUT2 'Plotting\RandomForest_crossvalidation_20fold_' Exps{xxx}], '-dpng','-r300');
    close;
end


%% k-folder impacts
kfolds = [5:5:50];

% close; figure(1)
% set(gcf,'pos',[32, 49,2316,1308]);
for xxx=length(Start_num):length(Start_num)
    figure(1)
    set(gcf,'pos',[32, 49,2316,1308]);
    
    for ii=1:length(Selected_sites)
        
        fprintf('ii=%.0d\n',ii);
        [INPUTS, INPUTS_NAMES] = xlsread([OUT Selected_sites{ii} '_Pred_Dep.xls'],'outputs');
        
        for jj=1:length(kfolds)
            YX         = [log(INPUTS(:,length(Selected_FGs)+CYA_start_Col)+1) INPUTS(:,[Start_num(xxx):End_num(xxx)])];
            %             YX         = [log(INPUTS(:,jj+7)+1) INPUTS(:,[5:6])];
            %             YX         = INPUTS(:,[jj+7, 5:6]);
            %         NUMS       = (isnan(YX(:,1))==1 | isnan(YX(:,2))==1 | isnan(YX(:,3))==1 | isnan(YX(:,4))==1);
            NUMS       = (isnan(YX(:,1))==1);
            Dates      = INPUTS(:,1:3); Dates(NUMS,:) = [];
            YX(NUMS,:) = [];
            
            kfold = kfolds(jj);
            
            ResponseV = RandomForest(YX, kfold); % ResponseV = [Y ySim yPred];
            ResponseV = ResponseV(:, [1, 3]);
            
            %             if exist('ResponseV')~=0
            %             SampleN(ii,jj)    = size(ResponseV,1);
            NUMS = length(find(isnan(ResponseV(:,1))==0 & isnan(ResponseV(:,2))==0));
            SampleN(ii,jj)    = NUMS;
            NSE(ii,jj)        = CE(ResponseV);
            KGE(ii,jj)        = klinggupta(ResponseV(:,2), ResponseV(:,1));
            TEMP              = corrcoef(ResponseV);
            r(ii,jj)          = TEMP(2,1);
            
            % plotting
            %         figure(1)
            subplot(length(Selected_sites),length(kfolds),(ii-1)*length(kfolds)+jj)
            plot(ResponseV(:,1),ResponseV(:,2),'r.','MarkerSize',8);
            xlim([0 max(ResponseV(:,1))*1.05]);
            ylim([0 max(ResponseV(:,1))*1.05]);
            hold on;
            plot([0 max(ResponseV(:,1))*1.05],[0 max(ResponseV(:,1))*1.05],'k');
            text(max(ResponseV(:,1))*0.1, max(ResponseV(:,1))*0.95,sprintf('NSE = %.2f',NSE(ii,jj)),'Fontsize',16);
            text(max(ResponseV(:,1))*0.1, max(ResponseV(:,1))*0.80,sprintf('N = %.0d',NUMS),'Fontsize',16);
            
            ResponseV_Matrix{ii,jj} = ResponseV;
            clear ResponseV Folder_kfold_S Folder_kfold_E;
            %             end
        end
        
    end
    
    save([OUT2 'RandomForest_crossvalidation_foldSens_' Exps{xxx}], 'NSE','KGE','SampleN','r','Selected_FGs','Selected_sites','Dates','ResponseV_Matrix');
    print ([OUT2 'Plotting\RandomForest_crossvalidation_foldSens_' Exps{xxx}], '-dpng','-r300');
    close;
end