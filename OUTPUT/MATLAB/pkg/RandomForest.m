%% YQZ, CLW, 2018.08.31
% Generating a forest function for data training (simulation) and crossvalidation (prediction)
%% kongdd; 17 Sep, 2018
% ----------------------
% 1. fixed the error of predictor selecting
function [ ResponseV ] = RandomForest(INPUTS,kfold)

%% Step1. reading dataset
X         = INPUTS(:,2:size(INPUTS,2));
Y         = INPUTS(:,1);
Ypred     = nan(size(Y));

%% Step2. training data using all data
% Establish regression tree ensemble (bagging, a generalised random forest method)
t         = templateTree('NumPredictorsToSample','all',...
    'PredictorSelection','interaction-curvature','Surrogate','on');

Md1       = fitrensemble(X,Y,'Method','Bag','NumLearningCycles',200,...
    'Learners',t);
% Simualtion
ySim      = predict(Md1,X);

%% Step3. k-fold cross-validation
if nargin >= 3 
    % Step3.1. to generate random number without replicates for all inputs data
    RandINDEX = randperm(size(X,1),size(X,1));

    % Step3.2. to pick up start and end row number
    for ll=1:kfold
        START (ll) = ceil(size(X,1).*(ll-1)/kfold)+1;
        END (ll) = ceil(size(X,1).*ll/kfold);
        INDEX{ll} = [START(ll):END(ll)]';
    end

    % Step3.3 step through all calibration/validation for kfold times
    ncol = size(INPUTS, 2);
    for ll = 1:kfold
        % Initialisation of INPUTS
        YX0 = INPUTS;
        
        % training data ((kfold-1)/kfold used for training
        YX0(RandINDEX(INDEX{ll}),:) = [];
        x0   = YX0(:,2:ncol);
        y0   = YX0(:,1);
        
        Md1       = fitrensemble(x0,y0,'Method','Bag','NumLearningCycles',200,...
            'Learners',t);
        % cross-validation data (1/kfold used for cross validation)
        x        = INPUTS(RandINDEX(INDEX{ll}),2:ncol);
        y        = INPUTS(RandINDEX(INDEX{ll}),1);
        
        % prediction
        yPred0   = predict(Md1,x);
        yPred (RandINDEX(INDEX{ll}),1)  = yPred0;
    end
end

%% Step4. Outputing observed, simulated and predicted response variable
ResponseV = [Y ySim yPred];
