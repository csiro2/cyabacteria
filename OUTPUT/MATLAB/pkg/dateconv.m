function date = dateconv(str)

try
    date = datetime(str, 'InputFormat', 'dd/MM/yyyy');
catch
    date = datetime('');
end