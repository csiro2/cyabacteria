function [ kge ] = klinggupta( modelled, observed)
%Nash Sutcliffe Efficiency measure

cflow=[modelled observed];

sdmodelled=std(modelled);
sdobserved=std(observed);
 
mmodelled=mean(modelled);
mobserved=mean(observed);

r=corr(cflow);
relvar=sdmodelled/sdobserved;
bias=mmodelled/mobserved;

%KGE timeseries 
kge=1-  sqrt( ((r(1,2)-1)^2) + ((relvar-1)^2)  + ((bias-1)^2) );
 
end


