function [CE]=CE(input)
% Calculation of Nash-Schutcliffe Coefficient of Efficiency according two
% arrays inputs
% last update on 08 Jan., 2008 by Yongqiang Zhang
% copyright CSIRO Land and Water

XX=input(:,1); % measured values
YY=input(:,2); % modelled values
temp_num=find(isnan(XX)==0 & isnan(YY)==0);
% XX(temp_num)=[];YY(temp_num)=[];

CE=1-sum((XX(temp_num)-YY(temp_num)).^2)./sum((XX(temp_num)-mean(XX(temp_num))).^2);
