%% YQZ, CLW, 23 July 2018
% Known issues:
% -------------
% 1. 4 unrelated sites were selected
%% Dongdong Kong, 17 Sep, 2018
% This script has been placed by 'd1_tidy_cyaBacteria.R', only 20 lines. 
clear;clc;close all;

%% environment setup

OUT = '\\osm-27-cdc.it.csiro.au\OSM_CBR_LW_HYDROFLUSH_work\Goyder\Cyanobacteria\Processed\';
DIR = '\\osm-27-cdc.it.csiro.au\OSM_CBR_LW_HYDROFLUSH_work\Goyder\Cyanobacteria\From_Maria_20180410\';
OUT = '\';

[cb, cb_names] = xlsread([DIR, 'ALL_DATA_and_TAXANOMIC_processed.xlsx']);

headline       = cb_names(1,:);
cb_names(1,:)  = [];
%% calculating dd mm yyyy

date  = cellfun(@dateconv, cb_names(:,6), 'UniformOutput', false);
% dates = cellstr(date);
dates   = cat(1, date{:});
yearly  = year(dates);
monthly = month(dates);
daily   = day(dates);



%% Get five sites to match with hydrological inputs dataset

[Sample_sites,ID] = unique(cb_names(2:size(cb_names,1),5));

Selected_sites = {'River Murray Morgan Sample Pump','River Murray Mannum 2 sites','River Murray Murray Bridge 2 sites',...
    'River Murray Tailem Bend 2 sites','River Murray Lock 9'};

OUTPUTS      = [];
OUTPUTS_NAME = [];
for ii= 1:length(Selected_sites)
    
    for jj = 1:length(Sample_sites)
        if strcmp(Selected_sites{ii},Sample_sites{jj})==1
            break;
        end
    end
    
    NUMS         = [ID(jj)+1:ID(jj+1)];
    TEMP         = [yearly(NUMS) monthly(NUMS) daily(NUMS) cb(NUMS,:)];
    OUTPUTS       = [OUTPUTS;TEMP];
    OUTPUTS_NAME = [OUTPUTS_NAME;cb_names(NUMS,:)];
    
end

% to assigne value for the matrix
for ii = 1:size(OUTPUTS_NAME,1)
    for jj=1:size(OUTPUTS,2)-4
        if length(OUTPUTS_NAME{ii,jj})==0 | isnan(OUTPUTS_NAME{ii,jj})==1
            OUTPUTS_NAME {ii,jj} = OUTPUTS(ii,jj+4);
        end
    end
end

%% cyanobacteria: functional groups

Selected_FGs = {'DIA','CHL','CYA','CYA_I','CYA_IIIb','CYA_IIIp','CYA_IVb','CYA_IVp'};

cyanob_matrix = zeros(length(OUTPUTS_NAME),1);

for ii= 1:length(OUTPUTS_NAME)
    for jj= 1:length(Selected_FGs)
        if strcmp(OUTPUTS_NAME{ii,11},Selected_FGs{jj})==1
            cyanob_matrix(ii,1) = 1;
            break;
        end
    end
end

OUTPUTS_NAME_Cyanob = OUTPUTS_NAME;
OUTPUTS_NAME_Cyanob(cyanob_matrix==0,:) = [];

xlswrite([OUT 'CYA_CHL_FiveSites.xls'],OUTPUTS_NAME_Cyanob,'outputs','a2');
xlswrite([OUT 'CYA_CHL_FiveSites.xls'],headline,'outputs','a1');

