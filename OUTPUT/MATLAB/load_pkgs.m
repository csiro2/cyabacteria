% load_pkgs;
%% environment setup
OUT = '\\osm-27-cdc.it.csiro.au\OSM_CBR_LW_HYDROFLUSH_work\Goyder\Cyanobacteria\Processed\';

%% putting together
Selected_FGs = {'CHL','CYA','CYA_I','CYA_IIIb','CYA_IIIp','CYA_IVb','CYA_IVp'};

Selected_sites = {'River Murray Morgan Sample Pump','River Murray Mannum 2 sites','River Murray Murray Bridge 2 sites',...
    'River Murray Tailem Bend 2 sites','River Murray Lock 9'};
