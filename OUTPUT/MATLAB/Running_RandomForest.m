%% YQZ, CLW, 18 July 2018

clear;clc;close all;
load_pkgs;

%% 
figure(1)
set(gcf,'pos',[32, 49,2316,1308]);

for ii=1:length(Selected_sites)
    
    fprintf('ii=%.0d\n',ii);
    [INPUTS, INPUTS_NAMES] = xlsread([OUT Selected_sites{ii} '_Pred_Dep.xls'],'outputs');
    
    for jj=1:length(Selected_FGs)
        
        % using Velocity and Tempearture as predictors
        YX         = [log(INPUTS(:,jj+7)) INPUTS(:,[5:6])];
        %         NUMS       = (isnan(YX(:,1))==1 | isnan(YX(:,2))==1 | isnan(YX(:,3))==1 | isnan(YX(:,4))==1);
        NUMS       = (isnan(YX(:,1))==1);
        YX(NUMS,:) = [];
        SampleN(ii,jj)    = size(YX,1);
        if size(YX,1)>10
            ResponseV         = RandomForest(YX);
            NSE(ii,jj)        = CE(ResponseV);
            KGE(ii,jj)        = klinggupta(ResponseV(:,2), ResponseV(:,1));
            TEMP              = corrcoef(ResponseV);
            r(ii,jj)          = TEMP(2,1);
            % plotting
            %         figure(1)
            subplot(length(Selected_sites),length(Selected_FGs),(ii-1)*7+jj)
            plot(ResponseV(:,1),ResponseV(:,2),'r.');
            xlim([0 max(ResponseV(:,1))*1.05]);
            ylim([0 max(ResponseV(:,1))*1.05]);
            hold on;
            plot([0 max(ResponseV(:,1))*1.05],[0 max(ResponseV(:,1))*1.05],'k');
            text(max(ResponseV(:,1))*0.2, max(ResponseV(:,1))*0.85,sprintf('NSE = %.2f',NSE(ii,jj)),'Fontsize',16);
            
            ResponseV_Matrix{ii,jj} = ResponseV;
        end
    end
    
end

save([OUT 'RandomForest_training'], 'NSE','KGE','SampleN','r','Selected_FGs','Selected_sites','ResponseV_Matrix');
print ([OUT 'Plotting\RandomForest_training'], '-dpng','-r300');

%% k-fold cross validation: 5-fold
close; figure(1)
set(gcf,'pos',[32, 49,2316,1308]);

kfold = 5;
for ii=1:length(Selected_sites)
    
    fprintf('ii=%.0d\n',ii);
    [INPUTS, INPUTS_NAMES] = xlsread([OUT Selected_sites{ii} '_Pred_Dep.xls'],'outputs');
    
    for jj=1:length(Selected_FGs)
        
        % using Velocity and Tempearture as predictors
        YX         = INPUTS(:,[jj+7, 5:6]);
        %         NUMS       = (isnan(YX(:,1))==1 | isnan(YX(:,2))==1 | isnan(YX(:,3))==1 | isnan(YX(:,4))==1);
        NUMS       = (isnan(YX(:,1))==1);
        YX(NUMS,:) = [];
        
        for ll=1:kfold
            %            if ll==1
            %                Folder_kfold_S (ll) = 1;% [1                  ceil(size(YX,1)/3)+1  ceil(size(YX,1).*2/3)+1];
            %                Folder_kfold_E (ll) = ceil(size(YX,1)/kfold);
            %            else
            Folder_kfold_S (ll) = ceil(size(YX,1).*(ll-1)/kfold)+1;
            Folder_kfold_E (ll) = ceil(size(YX,1).*ll/kfold);
            %            end
        end
        % Establish regression tree ensemble (bagging, a generalised random forest method)
        t         = templateTree('NumPredictorsToSample','all',...
            'PredictorSelection','interaction-curvature','Surrogate','on');
        for kk=1:length(Folder_kfold_E)
            
            YX0 = YX;
            
            X = YX(Folder_kfold_S(kk):Folder_kfold_E(kk),2:size(YX,2));
            Y = YX(Folder_kfold_S(kk):Folder_kfold_E(kk),1);
            
            YX0(Folder_kfold_S(kk):Folder_kfold_E(kk),:) = [];
            X0   = YX0(:,2:size(YX,2));
            Y0   = YX0(:,1);
            
            if size(YX0,1)>10
                
                % using Velocity and Tempearture as predictors
                Md1       = fitrensemble(X0,Y0,'Method','bag','NumLearningCycles',200,...
                    'Learners',t);
                % Prediction
                yPre      = predict(Md1,X);
                ResponseV (Folder_kfold_S(kk):Folder_kfold_E(kk),1) = Y;
                ResponseV (Folder_kfold_S(kk):Folder_kfold_E(kk),2) = yPre;
            end
        end
        
        if exist('ResponseV')~=0
            SampleN(ii,jj)    = size(ResponseV,1);
            NSE(ii,jj)        = CE(ResponseV);
            KGE(ii,jj)        = klinggupta(ResponseV(:,2), ResponseV(:,1));
            TEMP              = corrcoef(ResponseV);
            r(ii,jj)          = TEMP(2,1);
            
            % plotting
            %         figure(1)
            subplot(length(Selected_sites),length(Selected_FGs),(ii-1)*7+jj)
            plot(ResponseV(:,1),ResponseV(:,2),'r.');
            xlim([0 max(ResponseV(:,1))*1.05]);
            ylim([0 max(ResponseV(:,1))*1.05]);
            hold on;
            plot([0 max(ResponseV(:,1))*1.05],[0 max(ResponseV(:,1))*1.05],'k');
            text(max(ResponseV(:,1))*0.2, max(ResponseV(:,1))*0.85,sprintf('NSE = %.2f',NSE(ii,jj)),'Fontsize',16);
            
            ResponseV_Matrix{ii,jj} = ResponseV;
            clear ResponseV Folder_kfold_S Folder_kfold_E;
        end
    end
    
end


save([OUT 'RandomForest_crossvalidation'], 'NSE','KGE','SampleN','r','Selected_FGs','Selected_sites','ResponseV_Matrix');
print ([OUT 'Plotting\RandomForest_crossvalidation'], '-dpng','-r300');
