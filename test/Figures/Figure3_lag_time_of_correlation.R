library(corrplot)

df_sum  <- dcast(df_all, date+yd7+year+d7~site, value.var = "CYA_sum")

cor_mat <-  cor(df_sum[, -(1:4)], use = "pairwise.complete.obs")
res1    <- cor.mtest(df_sum[, -(1:4)], conf.level = .95)

## 1. lag time of downstream and upstream
corr_lst <- list()
lags <- c(1, 2, 3, 4, 5, 8, 10, 52)
for(j in seq_along(lags)){
    lag <- lags[j]
    I0 <- df_sum$yd7
    I  <- match(I0 - lag, I0)
    
    corr <- numeric()
    for (i in 1:(length(shortname) - 1)){
        x_up   <- df_sum[, .SD , .SDcols = shortname[i]][[1]]
        x_down <- df_sum[, .SD , .SDcols = shortname[i+1]][[1]]
        
        # r <- cor.test(x_up[I], x_down, use = "pairwise.complete.obs")
        # c(r$estimate[[1]], r$p.value)
        corr[i] <- cor(x_up[I], x_down, use = "pairwise.complete.obs")
    }
    corr_lst[[j]] <- corr %>% set_names(shortname[-1])
}

corr_lag <- do.call(rbind, corr_lst) %>% as.data.table() %>% cbind(lag = lags, .)
corr_lag
write.xlsx(corr_lag, "table1. lag time of corr.xlsx")

## 2. auto correlation
corr_lst <- list()
lags <- c(1, 2, 3, 4, 5, 8, 10, 52)
for(j in seq_along(lags)){
    lag <- lags[j]
    I0 <- df_sum$yd7
    I  <- match(I0 - lag, I0)
    
    corr <- numeric()
    for (i in 1:(length(shortname))){
        x_up   <- df_sum[, .SD , .SDcols = shortname[i]][[1]]
        # x_down <- df_sum[, .SD , .SDcols = shortname[i+1]][[1]]
        
        # r <- cor.test(x_up[I], x_down, use = "pairwise.complete.obs")
        # c(r$estimate[[1]], r$p.value)
        corr[i] <- cor(x_up[I], x_up, use = "pairwise.complete.obs")
    }
    corr_lst[[j]] <- corr %>% set_names(shortname)
}

corr_auto <- do.call(rbind, corr_lst) %>% as.data.table() %>% cbind(lag = lags, .)
corr_auto
write.xlsx(corr_auto, "table1. lag time of autocorr.xlsx")
