source('R/load_pkgs.R')

predictors = c("date", "Velocity", "Temperature", "Salinity", "Discharge") # , "doy"
response   = c("CYA_IIIp") #CYA_IIIp, CYA_sum

# df <- df[CYA_sum > log10(2e4)]
# df_INPUT <- .SD, .SDcols = c(predictors, response)]
# INPUT <- randomForest_data(df, predictors, response, train_perc = 0.75)
# # m     <- with(INPUT, randomForest(x_train, y_train, x_test, y_test, ntree = 500))
# m     <- with(INPUT, randomForest(X, Y, ntree = 500))
# m$importance

lst <- df %>% split(., .$site) # divide into sites
# data <- lst$Morgan

trs      <- c(0, 100) 
titlestr <- sprintf("threshold = %d", trs)
inds <- list(1:4) # 2:5, c(1, 2:5)
pred <- list()

for (j in seq_along(trs)){
    trs_j <- trs[j] %>% log10() # convert to log10
    for (i in seq_along(inds)){
        predictors_i <- predictors[inds[[i]]]
        print(predictors_i)
        
        res  <- llply(lst, predict_bacteria, 
                      predictors = predictors_i, threshold = trs_j,
                      .progress = "text")
        info <- map(res, "info") %>% {melt_list(., "site")[kford == "all"]}
        print(info[, .(NSE, R2, MAE, RMSE, Bias, n_sim, site)])
        
    }
    pdat <- map(res, "output") %>% melt_list("site") %>% 
        melt(id.vars = c("t", "site"), variable.name = "kind")
    pred[[j]] <- pdat
}

pred %<>% set_names(trs)

df_pred <- melt_list(pred, "trs") %>% 
    dcast(t+site+trs~kind, value.var="value") 
df_pred$site %<>% factor(shortname)
# %>% dcast(t+site+yObs~trs, value.var="ySim")

ggplot(df_pred, aes(yObs, ySim)) + geom_point() + 
    geom_abline(slope = 1, color = "red") +
    facet_grid(site~trs)
    

p_trs <- ggplot(df_pred, aes(t, yObs)) + 
    geom_hline(yintercept = log10(1000), linetype = 2, color = "red")+
    geom_hline(yintercept = log10(5000), linetype = 1, color = "red") +
    geom_line() + 
    # geom_point() +
    geom_line(aes(t, ySim, color = trs)) +
    labs(x = "Time", y = expression(log10(CYA[sum]) * " cells*"*ml^-1)) + 
    guides(color = guide_legend(title = "threshold")) + 
    facet_wrap(~site, nrow = 5, strip.position="right", scales = "free_y") 
    

CairoPDF("Figure1_CYA_sum filter by different threshold.pdf", 11, 8) 
print(p_trs)
dev.off()
## 1. "Velocity", "Temperature", "Salinity"
# p <- ggplot(df, aes(Date, CYA_sum, color = site)) 
# ggplotly(p)

## p1 data.check
p1 <- ggplot(pdat, aes(t, value, color = kind)) + 
    geom_line() + 
    facet_wrap(~site, nrow = 5, strip.position="right", scales = "free_y") + 
    labs(x = "Time", y = expression(log(CYA[sum]) * " (cells/ml)")) + 
    ggtitle(titlestr)
    # scale_y_continuous(limits = c(0, 14))
# ggplotly(p1)    


## tem
# p2 <- ggplot(df, aes(Date, Temperature, color = site)) + 
#     geom_line() + facet_wrap(~site, nrow = 5, strip.position="right")
# ggplotly(p2) 

# pls regression ----------------------------------------------------------

## 1. Check data dist first
# library(lmom)
# llply(lst, . %$% samlmu(CYA_sum)) %>% 
#     do.call(rbind, .) %>% 
#     lmrd()
# library(pls)
# d <- lst$`Lock 9`
# l <- mvr(CYA_sum~date+Velocity+Temperature+Salinity, 3, data = d, 
#         scale = T, center = T)
# ypred <- predict(l, d)
# 
# GOF(d$CYA_sum, ypred[,,3])
# 
# plot(d$CYA_sum, type = "l"); grid()
# lines(ypred[,,3], col = "blue")
# dim(l$fitted.values)