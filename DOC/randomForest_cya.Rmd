---
title: "Cyanobacteria prediction with Random Forest"
author: "Dongdong Kong"
date: "17 September 2018"
output:
  slidy_presentation:
    fig_height: 10
    fig_width: 16
    keep_md: yes
    smaller: yes
    widescreen: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

## Modelling experiments

- **Previous model:**  
CYA ~ Velocity+Temperature+Salinity

- **New variables:**  
1. Date, subtract origin (e.g. 1970-01-01) and convert to numeric  
2. day of year(doy)  
3. Discharge  

- **Variable importance:**  
Date > Discharge > Velocity > Salinity > doy


```{r 1.load pkg and data, message=FALSE}
library(randomForest)
library(caret)
library(phenofit)
library(DT)
library(plotly)
# library(keras)

source('R/load_pkgs.R')

predictors = c("date", "doy", "Velocity", "Temperature", "Salinity", "Discharge")
response   = c("CYA_sum")

df <- fread("data/INPUT_randomForest.csv")
df$Date %<>% ymd()
df[, `:=`(date = as.numeric(Date), doy = yday(Date))]

# CYA_sum =0 means no measurement
df <- df[CYA_sum > 1, ]
df$CYA_sum %<>% log()
# df_INPUT <- .SD, .SDcols = c(predictors, response)]

lst <- df %>% split(., .$site) # divide into sites
```

## 1. preparing INPUT data  
```{r cars, echo=FALSE}
DT::datatable(df[1:100, ])  %>% 
              formatRound(columns = 3:15, digits = 1)
```

## Check Input Data
```{r}
d2 <- fread("data/INPUT_randomForest2.csv")
d2$Date %<>% ymd()
p <- ggplot(d2, aes(Date, log(CYA_sum), color = longname)) + 
    geom_line() + 
    facet_wrap(~site, ncol = 1) 
ggplotly(p)
```

## Random Forest with different predictors

```{r}
inds <- list(3:5, 3:6, c(2, 3:6), c(1, 3:6))
```
```{r models, include=FALSE, eval=FALSE}
res  <- list()
for (i in seq_along(inds)){
    predictors_i <- predictors[inds[[i]]]
    #print(predictors_i)
    res[[i]]  <- suppressWarnings(llply(lst, pred_Bacteria, predictors = predictors,.progress = "text"))
    # info <- map(res, "info") %>% {melt_list(., "site")[kford == "all"]}
    # DT::datatable(info[, .(NSE, R2, MAE, RMSE, Bias, n_sim, site)])
}
save(res, file = "data/RF_kford.rda")
```
```{r}
load("data/RF_kford.rda")
```


```{r, comment="#"}
for (i in seq_along(inds)){
    predictors_i <- predictors[inds[[i]]]
    print(predictors_i)

    info <- map(res[[i]], "info") %>% {melt_list(., "site")[kford == "all"]}
    # htmltools::tagList(
        DT::datatable(info[, .(NSE, R2, MAE, RMSE, Bias, n_sim, site)]) %>% print() #(
    # ) %>% print
}
```

## Result
```{r}
pdat <- map(res[[i]], "output") %>% melt_list("site") %>% 
    melt(id.vars = c("t", "site"), variable.name = "kind")

## p1 data.check
p2 <- ggplot(pdat, aes(t, value, color = kind)) + 
    geom_line() + facet_wrap(~site, nrow = 5, strip.position="right")
ggplotly(p2)    
```


